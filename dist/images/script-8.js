$(document).ready(function() {

	$(document).on('click', '#regform .registration__btn', function(e) {
		e.preventDefault();
		
		var formData = $('#regform').serialize() +'&register_submit_button=Y';
		sendAjaxReg(formData);
	});
	
	$(document).on('click', '#regform .get-code__btn', function(e) {
		e.preventDefault();
		
		var formData = $('#regform').serialize() + '&register_submit_button=Y' + '&GET_CODE=Y';
		sendAjaxReg(formData);
	});
});

function sendAjaxReg(data) {
	$.ajax({
		type: 'post',
		url: '/ajax/registerForm.php',
		data: data,
		success: function (data) {
			$('#registrationForm').html(data);
			
			document.body.dispatchEvent(new CustomEvent("formAjaxCall", {
					detail: {
						popupId: 'registrationForm'
					}
				})
			);
		}
	});
}