$(document).ready(function() {
	
    $("body").on("click", "#"+formIdMob+" .js-sign-in__submit", function(e) {
		e.preventDefault();
		
		var formDataMob = $('#'+formIdMob+'').serialize() +'&auth_submit_button=Y';

		if($(this).hasClass("code")) {
			// Первое нажатие STEP_1
			formDataMob = formDataMob + '&step=1';
		} else {
			// Второе нажатие STEP_2
			formDataMob = formDataMob + '&step=2';
		}
		
		
		$.ajax({
			type: 'post',
			url: '/ajax/authForm.php',
			data: formDataMob,
			success: function (data) {
				//console.log(data);
				$('#'+containerIdMob+'').html(data);
				
				if ($('#'+formIdMob+' [name="STEP_2"]').val() == 'Y') {
					$('#'+formIdMob+' .js-sign-in__submit').removeClass("code");
					$('#'+formIdMob+' .form__item.code').slideDown();
					//$('#'+formIdMob+' .js-sign-in__submit').removeClass("code").siblings(".form__item.code").slideDown();
				}
				
				document.body.dispatchEvent(new CustomEvent("formAjaxCall", {
						detail: {
							popupId: containerIdMob
						}
					})
				);
			}
		});

    });
	
});
	
