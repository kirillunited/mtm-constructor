window.addEventListener("beforeunload", function(){
	var date = new Date(0);
	document.cookie = "active_item=; path=/; expires=" + date.toUTCString();
});
function getCookie(name) {
	var matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	return matches ? decodeURIComponent(matches[1]) : undefined;
}
	
function showClothesPopup() {
	$(".popups").show();
	$(".cloth__popup").show();
	$("body").addClass("popup__open");
	return false;
}

function initSlider() {
	if($('.popup__slider').length > 0) {
        $('.popup__slider').slick({
            fade: true,
            infinite: true,
            prevArrow: '<div class="banner__arrow banner__prev"></div>',
            nextArrow: '<div class="banner__arrow banner__next"></div>',
        });
    }
}

function getFabricData(itemID, func) {
	$.ajax({
		url: '/ajax/fabric_item.php',
		dataType: 'json',
		data: {
			ELEMENT_ID: itemID,
			AJAX_FABRIC: "Y"
		},
		success: function(data) {
			if (!data.IMAGES_SHIRT) {
				$('.info__btns .shirt__btn').hide();
			} else {
				$('.info__btns .shirt__btn').show();
			}
			
			if($(data.HTML).hasClass("popup_item")) {
				$(".cloth__popup").html($(data.HTML).html());
				initSlider();
			}
			
			if(data.IMG && data.IMG.length > 0) {
				$(".zoom__btn").attr("href", data.IMG);
			}
			
			if(data.PRICE && parseInt(data.PRICE) > 0) {
				$(".price__info span").html(data.PRICE + " руб.");
			}
			
			if(data.NAME && data.NAME.length > 0) {
				$(".cloth__info span").html(data.NAME);
			}
			
			if(data.MANUFACTURER_NAME && data.MANUFACTURER_NAME.length > 0) {
				$(".cloth__factory span").html(data.MANUFACTURER_NAME);
			}
		}
	});
}

$(document).ready(function() {
	
	$("body").on("click", ".cloth__list .cloth__item.js-constructor__option", function() {
		var date = new Date(new Date().getTime() + 3600 * 1000);
		var itemID = $(this).attr('data-val');
		document.cookie = "active_item=" + itemID + "; path=/; expires=" + date.toUTCString();
	});
	
	$("body").on("click", ".js-cloth__btn", function() {
		var buttonType = $(this).attr('data-type');
		if (buttonType == 'cloth_btn') {
			if (!$('div.cloth_btn').hasClass('showPop')) {
				$('div.cloth_btn').removeClass('hidePop');
				$('div.cloth_btn').addClass('showPop');
			}
			if (!$('div.shirt_btn').hasClass('hidePop')) {
				$('div.shirt_btn').removeClass('showPop');
				$('div.shirt_btn').addClass('hidePop');
			}
		} else if (buttonType == 'shirt_btn') {
			if (!$('div.shirt_btn').hasClass('showPop')) {
				$('div.shirt_btn').removeClass('hidePop');
				$('div.shirt_btn').addClass('showPop');
			}
			if (!$('div.cloth_btn').hasClass('hidePop')) {
				$('div.cloth_btn').removeClass('showPop');
				$('div.cloth_btn').addClass('hidePop');
			}
		}
		
        $(".popups").show();
        $(".cloth__popup").show();
        $("body").addClass("popup__open");
        return false;
    });
	
	
    $("body").on("click", ".cloth__select .select__item  a", function() {
    	var that = $(this);
        that.closest(".submenu").siblings(".select__field").text(that.text());
        that.closest(".submenu").hide().parent().removeClass("open");
        return false;
    });

	// Загрузка инф. по выбранной ткани
	//var activeItem = $(".cloth__item.active"),
		//itemID = activeItem.find(".item__link").data("id");
	var itemID = $('.constructor__item .item_info').attr('data-itemID');
	if(parseInt(itemID) > 0) {
		getFabricData(itemID);
	}
	
	$(document).on("click", ".item__link", function(e) {
		e.preventDefault();
		showClothesPopup();
	});
	
	// Дет. инф. по клику на ткань
	$(document).on("click", "#t1s1 .cloth__item", function(e) {
		e.preventDefault();
		var itemID = $(this).find(".item__link").data("id");
		getFabricData(itemID);
	});
});

