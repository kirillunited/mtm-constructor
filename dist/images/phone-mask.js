function phoneMask(){
	
	// Формируем "чистый" номер без кода для маски
	var inputsAll = $(".user__phone input");
	var correctPhoneNum = 0;
	var userCountry = '';
	for (i = 0; i < inputsAll.length; i++) {
		var phoneNumLoad = $(inputsAll[i]).attr('value');
		if (phoneNumLoad) {
			if (phoneNumLoad.substring(0, 1) == '7') { // RU
				userCountry = 'RU';
				correctPhoneNum = phoneNumLoad.substring(1);
			} else if (phoneNumLoad.substring(0, 3) == '375') { // BY
				userCountry = 'BY';
				correctPhoneNum = phoneNumLoad.substring(3);
			} else if (phoneNumLoad.substring(0, 3) == '380') { // UA
				userCountry = 'UA';
				correctPhoneNum = phoneNumLoad.substring(3);
			}
			
			$(inputsAll[i]).attr('value', correctPhoneNum);
			//$(inputsAll[i]).attr('value', 447669514);
			//$(inputsAll[i]).inputmask("setvalue", 37512345678901); 
		}
	}
	
				
	var maskList = $.masksSort($.masksLoad("https://cdn.rawgit.com/andr-04/inputmask-multi/master/data/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
	var country = 'RU';
	Inputmask("+7(###)###-##-##").mask($(".user__phone input"));
	//$(".user__phone input").val("7");
	

	
	var maskOpts = {
		inputmask: {
			definitions: {
				'#': {
					validator: "[0-9]",
					cardinality: 1
				}
			},
			//showMaskOnHover: true,
			showMaskOnHover: false,
			autoUnmask: false,
			clearMaskOnLostFocus: false,
			onBeforeMask: function (pastedValue, opts) {
				var processedValue = pastedValue;
				if (correctPhoneNum && (processedValue == 99 || processedValue == 88)) {
					processedValue = correctPhoneNum;
				}
				return processedValue;
			}
		},
		match: /[0-9]/,
		replace: '#',
		list: maskList,
		listKey: "mask",
		onMaskChange: function(maskObj, determined) {
			if (determined) {
				var hint = maskObj.name_en;
				if (maskObj.desc_en && maskObj.desc_en != "") {
					hint += " (" + maskObj.desc_en + ")";
				}
				$("#descr").html(hint);
				} else {
				$("#descr").html("Mask of input");
			}
		}
	};
	/*
	if(Promise){
		var promise = new Promise( function(resolve, reject){
			
			$.getJSON('https://ipinfo.io', function(data){
				country = data.country;
				resolve(data.country);
			});
		});	
		
		$(".user__phone input").ready(function(e){
			
			promise.then(function(country){
				
				switch(country){
					case 'BY':
					Inputmask("+375(##) ###-##-##").mask($(".user__phone input"));
					break;
					case 'RU':
					Inputmask("+7(###)###-##-##").mask($(".user__phone input"));
					break;
					case 'KZ':
					Inputmask("+7(###)###-##-##").mask($(".user__phone input"));
					break;
					case 'UA':
					Inputmask("+380(##) ###-##-##").mask($(".user__phone input"));
					break;
					default:
					Inputmask("+7(###)###-##-##").mask($(".user__phone input"));
				}
			});	
		});
	}*/
	//else{
		$.getJSON('https://ipinfo.io', function(data){
			
			if (userCountry.length > 0) {
				var country = userCountry;
			} else {
				var country = data.country;
			}

			switch(country){
				case 'BY':
				Inputmask("+375(##)###-##-##").mask($(".user__phone input"));
				break;
				case 'RU':
				Inputmask("+7(###)###-##-##").mask($(".user__phone input"));
				break;
				case 'KZ':
				Inputmask("+7(###)###-##-##").mask($(".user__phone input"));
				break;
				case 'UA':
				Inputmask("+380(##)###-##-##").mask($(".user__phone input"));
				break;
				default:
				Inputmask("+7(###)###-##-##").mask($(".user__phone input"));
			}
		});
	//}
	$(".user__phone input").inputmask("remove");
	$(".user__phone input").inputmasks(maskOpts);
	$('#phone_mask').change();

	//-------------------------------------------
	document.body.addEventListener("formAjaxCall", function(event){
	
		//console.log($("#" + event.detail.popupId + " .user__phone input"));
		$("#" + event.detail.popupId + " .user__phone input").inputmasks(maskOpts);
	});
	//-------------------------------------------
}
document.addEventListener('DOMContentLoaded', phoneMask);