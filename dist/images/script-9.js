$(document).ready(function() {
	
	$(document).on('click', '#size_prof_form_add .add_button', function(e) {
		e.preventDefault();

		var formData = $('#size_prof_form_add').serialize() + '&add_size=Y';
		sendAjaxAdd(formData);
	});
	
	function sendAjaxAdd(data) {
		$.ajax({
			type: 'post',
			url: '/ajax/personal_size_profiles_add.php',
			data: data,
			success: function (data) {
				$('#personal_size_profiles_form_add').html(data);
			}
		});
	}
	
});


