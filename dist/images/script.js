function getHtml(tabID, succ) {
    var filePath = "/local/components/manao/construct/templates/.default/ajax/",
        ajaxFileName = tabID.replace("#", "") + ".php";

    $.ajax({
        url: filePath + ajaxFileName,
        success: succ,
		data: {'LOAD_ITEM': arLoadItem}
    });
}

function initConstructorSlider() {
	if($('.constructor__options.constructor__slider').length > 0) {
        $('.constructor__options.constructor__slider').not(".slick-initialized").slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            prevArrow: '<div class="slider__small_arrow slider__small_prev"></div>',
            nextArrow: '<div class="slider__small_arrow slider__small_next"></div>',
        });
    }
}

function setTab2() {
	if ($("#t2s1").hasClass("active")) {
		$(".constructor__item").find(".item__details_img").addClass("active");
		$(".constructor__item").find(".embroidery__style").removeClass("active");
		$(".constructor__item").find(".button__style").addClass("active");
	} else if ($("#t2s2").hasClass("active")) {
		$(".constructor__item").find(".item__details_img").addClass("active");
		$(".constructor__item").find(".button__style").removeClass("active");
		$(".constructor__item").find(".embroidery__style").addClass("active");
	} else if (
		!$("#t2s1").hasClass("active") 
		&& !$("#t2s2").hasClass("active") 
		&& $(".constructor__item").find(".item__details_img img").hasClass("active")
	) {
		$(".constructor__item").find(".item__details_img").removeClass("active");
		$(".constructor__item").find(".button__style").removeClass("active");
		$(".constructor__item").find(".embroidery__style").removeClass("active");
	}
}

// Расставляет исключения
function setIncompatibility(arrInc) {
	if (arrInc.length > 0) {
		var arIncHide = {
			'monogram' : 'monogram',
			'bottom__cuff' : 'cuff_bottom',
			'cuff' : 'cuff',
			'monogram__color' : 'monogram_color',
			'654' : 'monogram_text'
		};
		arrInc.forEach(function(item, i, arrInc) {
			if (!isNaN(item)) {
				// Белые манжеты
				if (item == 597) {
					$('.params__item.white_cuff').addClass('disabled');
				}
				// Текст вышивки
				if (item == 654) {
					$('.construct_form .params__item.' + arIncHide[item]).addClass('disabled');
					//$('.construct_form .params__item.' + arIncHide[item]).slideUp();
				}
				// Снимаем чекбоксы белого воротника и манжет
				if (item == 597) {
					$('input[data-id="' + item + '"]').prop("checked", false);
					//$('.params__item.white_cuff .params__value').html('Нет');
					//$('[name="white_cuff"]').attr('value', '0');

					$('.params__item.white_cuff .select__field.white_cuff_elem').attr('data-opt', 'N');
					$('.params__item.white_cuff .select__field.white_cuff_elem').val('Нет');
					$('input.param_input[name="white_cuff"]').val('0');
				}
				if (item == 598) {
					$('input[data-id="' + item + '"]').prop("checked", false);
					//$('.params__item.white_collar .params__value').html('Нет');
					//$('[name="white_collar"]').attr('value', '0');

					$('.params__item.white_collar .select__field.white_collar_elem').attr('data-opt', 'N');
					$('.params__item.white_collar .select__field.white_collar_elem').val('Нет');
					$('input.param_input[name="white_collar"]').val('0');
				}
				
				// Ставим белые квадраты ткани воротника и манжет
				if (item == 607) {
					$('.cloth-choice.js-collar_fabric .js-choice-btn').addClass('white-cloth');
					$('.construct_form .params__item.collar_fabric').addClass('disabled');
				}
				if (item == 608) {
					$('.cloth-choice.js-cuff_fabric .js-choice-btn').addClass('white-cloth');
					$('.construct_form .params__item.cuff_fabric').addClass('disabled');
				}
				
				if ($('[data-val="' + item + '"]').length > 0) {
					$('[data-val="' + item + '"]').addClass('disabled');
				} else if ($('[data-id="' + item + '"]').length > 0) {
					$('[data-id="' + item + '"]').attr('disabled', true);
				}
				$('.construct_form a.submenu__item[data-id-val="' + item + '"]').addClass('disabled');
				$('[data-id-val="' + item + '"]').addClass('disabled');
			} else {
				//$('.construct_form .params__item.' + arIncHide[item]).slideUp();
				$('.construct_form .params__item.' + arIncHide[item]).addClass('disabled');
				$('.' + item + ' .js-constructor__option').addClass('disabled');
			}
		});
	}
}

function sendAjaxToUpdateImg(dataLoad) {
	
	// if (dataLoad === undefined) {
		// thisData = $(".construct_form").serialize();
	// } else {
		// thisData = dataLoad;
	// }
	thisData = $(".construct_form").serialize();
	$.ajax({
		url: "/ajax/update_img.php",
		dataType: 'json',
		data: thisData,
		//data: $(".construct_form").serialize(),
		//data: 'construct_ajax=Y&save_wo_size=N&image=%2Fupload%2Fimg_shirts%2F806%2Fe925d4fef81c77c28d6adfd2a38ef160.png&price=5950&incompatibility=297%2C298%2C312%2C556%2Cmonogram%2Cmonogram__color%2C654&fabric=844&volume=260&clasp=274&collar=276&collar_add=0&sleeve=283&cuff=291&cuff_bottom=292&back=296&pocket=299&decor_add=0&collar_fabric=844&cuff_fabric=844&white_collar=0&white_cuff=0&buttons=773&embroidery_place=302&monogram=306&monogram_text=0&monogram_color=1015',
		//data: 'construct_ajax=Y&save_wo_size=N&image=%2Fupload%2Fimg_shirts%2F806%2Fe925d4fef81c77c28d6adfd2a38ef160.png&price=5950&fabric=804&volume=260&clasp=274&collar=281&collar_add=0&sleeve=283&cuff=291&cuff_bottom=292&back=296&pocket=299&decor_add=0&collar_fabric=844&cuff_fabric=844&white_collar=0&white_cuff=0&buttons=773&embroidery_place=302&monogram=306&monogram_text=0&monogram_color=1015',
		success: function(res) {
			if(typeof res.RESULT_IMAGE != "undefined" && res.RESULT_IMAGE.length > 0) {
				$(".constructor__item .item__img img").attr("src", res.RESULT_IMAGE);
				$('.construct_form input[name="image"]').attr('value', res.RESULT_IMAGE);
				$(".constructor__item .item__details_img").css("background-image", "url(" + res.FABRIC_IMAGE + ")");
			}
			if(typeof res.PRICE != "undefined" && parseInt(res.PRICE) > 0) {
				$(".constructor__item .price__info span").html(res.PRICE + " руб.");
				$('.construct_form input[name="price"]').attr('value', res.PRICE);
			}
			if(typeof res.MONOGRAMM_IMAGE != "undefined" && res.MONOGRAMM_IMAGE.length > 0) {
				$('.item__details_img .embroidery__style').attr('src', res.MONOGRAMM_IMAGE);
			}
			
			if(typeof res.FABRIC_IMAGE_COLLAR != "undefined" && res.FABRIC_IMAGE_COLLAR.length > 0) {
				$('.cloth-choice.js-collar_fabric .js-choice-btn img').attr('src', res.FABRIC_IMAGE_COLLAR);
				$('.js-collar_fabric .js-constructor__option.active').removeClass('active');
				$('.js-collar_fabric div[data-val="' + res.arItemsID_fabricAdd.collar_fabric + '"]').addClass('active');
			}
			if(typeof res.FABRIC_IMAGE_CUFF != "undefined" && res.FABRIC_IMAGE_CUFF.length > 0) {
				$('.cloth-choice.js-cuff_fabric .js-choice-btn img').attr('src', res.FABRIC_IMAGE_CUFF);
				$('.js-cuff_fabric .js-constructor__option.active').removeClass('active');
				$('.js-cuff_fabric div[data-val="' + res.arItemsID_fabricAdd.cuff_fabric + '"]').addClass('active');
			}
			
			// Очистим все элементы от disabled
			$('.constructor__option.js-constructor__option.disabled').removeClass('disabled');
			$('.embroidery-text.disabled').removeClass('disabled');
			$('.additional__options input').attr('disabled', false);
			$('.additional__options .cloth-choice.disabled').removeClass('disabled');
			//$('.construct_form a.submenu__item').removeClass('disabled');
			$('.construct_form').find('.disabled').removeClass('disabled');
			
			// Снимаем белые квадраты ткани воротника и манжет
			$('.cloth-choice.js-collar_fabric .js-choice-btn').removeClass('white-cloth');
			$('.cloth-choice.js-cuff_fabric .js-choice-btn').removeClass('white-cloth');
			
			// Сделаем видимыми все элементы финальной формы.
			//$('.construct_form .params__item').slideDown();
			
			if(typeof res.INCOMPATIBILITY != "undefined" && res.INCOMPATIBILITY.length > 0) {
				setIncompatibility(res.INCOMPATIBILITY);
				$('[name="incompatibility"]').attr('value', res.INCOMPATIBILITY);
			}
		}
	});
	
	if (dataLoad == 'load' && arChanges) {
		for(var k in arChanges) {
			if (k == 'fabric') {// Ткань
				fabricMess = 'Ткань "'+arChanges[k].NAME+'" сейчас недоступна. Мы заменили её на другую.<br>';
				$('.not_available_options').append(fabricMess);
			}
		}
	}
}

function saveWOSize(cond) {
	$('[name="save_wo_size"]').val(cond);
	return true;
} 

$(document).ready(function () {

    $("body").on("click", ".primary__tabs_link", function () {
        var link = $(this).attr("href");
        if (!$(this).hasClass("active")) {
            $(".primary__tabs_link").removeClass("active");

            $(this).addClass("active");

            $(".primary__tabs_item").removeClass("active");

            $(".secondary__tabs_link").removeClass("active");

			var tab = $(link).find(".secondary__tabs_item").length > 0 ? 
				$(link).addClass("active").find(".secondary__tabs_item").eq(0): $(link).addClass("active");
            function changeTab() {
                if ($(link).find(".secondary__tabs_link")) {
                    $(link).find(".secondary__tabs_link").eq(0).addClass("active");
                }
                $(".secondary__tabs_item").removeClass("active");
                tab.addClass("active");
				
				initConstructorSlider();
				$('.constructor__options.constructor__slider').slick('setPosition');
            }
			
            if(tab.length > 0 && tab.html().trim().length == 0) {
                getHtml(tab.attr("id"), function(data) {
                    tab.html(data);
                    changeTab();
                });
            } else {
                changeTab();
            }

			
        }
		setTimeout('setTab2()', 100);
		//setTab2();

        return false;
    });

    $("body").on("click", ".secondary__tabs_link", function () {
        var link = $(this).attr("href");
        if (!$(this).hasClass("active")) {
            $(".secondary__tabs_link").removeClass("active");

            $(this).addClass("active");

            $(".secondary__tabs_item").removeClass("active");

            if($(link).length > 0 && $(link).html().trim().length == 0) {
                getHtml(link, function(data) {
                    $(link).html(data);
                    $(link).addClass("active");
					initConstructorSlider();
					$(link).find('.constructor__options.constructor__slider').not('.slick-initialized').slick('setPosition');
                });
            } else {
                $(link).addClass("active");
				initConstructorSlider();
				$('.constructor__options.constructor__slider').slick('setPosition');
            }
            
        }
        setTimeout('setTab2()', 100);
		//setTab2();

        return false;
    });

    $("body").on("click", ".js-prev__step_btn", function () {

        if ($(".secondary__tabs_item.active").prev().hasClass("secondary__tabs_item")) {

            var tab = $(".secondary__tabs_item.active").removeClass("active").prev(".secondary__tabs_item");
			hideNextBtn(tab.attr("id"));
            function showTab() {
                $(".secondary__tabs_link.active").removeClass("active").prev(".secondary__tabs_link").addClass("active");
                tab.addClass("active");
				initConstructorSlider();
                $('.constructor__options.constructor__slider').slick('setPosition');
            }
            if(tab.length > 0 && tab.html().trim().length == 0) {
                getHtml(tab.attr("id"), function(data) {
                    tab.html(data);
                    showTab();
                });
            } else {
                showTab();
            }

        } else if ($(".primary__tabs_item.active").prev().hasClass("primary__tabs_item")) {

            var tab = $(".primary__tabs_item.active").removeClass("active").prev(".primary__tabs_item");
			hideNextBtn(tab.attr("id"));
            function showTab() {
                $(".secondary__tabs_link").removeClass("active");
                $(".secondary__tabs_item").removeClass("active");
                $(".primary__tabs_link.active").removeClass("active").prev(".primary__tabs_link").addClass("active");
                tab.addClass("active");
                $(".primary__tabs_item.active").find(".secondary__tabs_link").last().addClass("active");
                $(".primary__tabs_item.active").find(".secondary__tabs_item").last().addClass("active");
				initConstructorSlider();
                $('.constructor__options.constructor__slider').slick('setPosition');
            }

			var secondTab = tab.find('.secondary__tabs_item').last();
            if(secondTab.length > 0 && secondTab.html().trim().length == 0) {
                getHtml(secondTab.attr("id"), function(data) {
                    secondTab.html(data);
                    showTab();
                });
            } else {
                showTab();
            }

        }
        setTimeout('setTab2()', 100);
		//setTab2();

        return false;
    });

    $("body").on("click", ".js-next__step_btn", function () {

        if ($(".secondary__tabs_item.active").next().hasClass("secondary__tabs_item")) {

            var tab = $(".secondary__tabs_item.active").removeClass("active").next(".secondary__tabs_item");
			hideNextBtn(tab.attr("id"));
            function showTab() {
                $(".secondary__tabs_link.active").removeClass("active").next(".secondary__tabs_link").addClass("active");
                tab.addClass("active");
				initConstructorSlider();
                $('.constructor__options.constructor__slider').slick('setPosition');
            }
			//console.log(tab);
            if(tab.length > 0 && tab.html().trim().length == 0) {
                getHtml(tab.attr("id"), function(data) {
                    tab.html(data);
                    showTab();
                });
            } else {
                showTab();
            }


        } else if ($(".primary__tabs_item.active").next().hasClass("primary__tabs_item")) {

            var tab = $(".primary__tabs_item.active").removeClass("active").next(".primary__tabs_item");
			hideNextBtn(tab.attr("id"));
            function showTab() {
                $(".secondary__tabs_link").removeClass("active");
                $(".secondary__tabs_item").removeClass("active");
                $(".primary__tabs_link.active").removeClass("active").next(".primary__tabs_link").addClass("active");
                tab.addClass("active");
                $(".primary__tabs_item.active").find(".secondary__tabs_link").eq(0).addClass("active");
                $(".primary__tabs_item.active").find(".secondary__tabs_item").eq(0).addClass("active");
				initConstructorSlider();
                $('.constructor__options.constructor__slider').slick('setPosition');
            }
			
			var secondTab = tab.find('.secondary__tabs_item').first();
			if(secondTab.length > 0 && secondTab.html().trim().length == 0) {
                getHtml(secondTab.attr("id"), function(data) {
                    secondTab.html(data);
                    showTab();
                });
            } else {
                showTab();
            }
        }
        setTimeout('setTab2()', 100);
		//setTab2();
		
        return false;
    });

    $("body").on("click", ".additional__options .submenu a", function () {
		sendAjaxToUpdateImg();
	});
	
    $("body").on("click", ".js-constructor__option", function () {
		var that = $(this);
		$('.not_available_options').html('');
			
        if (!that.hasClass("active")) {
            that.parent().find(".js-constructor__option").removeClass("active");
            that.addClass("active");
			var propCode = that.closest(".constructor__options-new").siblings(".constructor__option_head").data("code"),
				propWrap = $(".params__item." + propCode),
				fabricID = propWrap.find(".param_input").val();
				
				//console.log(propCode);
				//console.log(propWrap);
			
			//select__field
			if(propWrap.find(".params__value .select__field").length > 0) {
				propWrap.find(".params__value .select__field").val(that.find(".item__name").html().trim());
			} else {
				propWrap.find(".params__value").html(that.find(".item__name").html().trim());
			}
			
			propWrap.find(".param_input").val(that.data("val"));
			
			// Drawing buttons images
			if (that.parents('.buttons').length) {
				var buttonImg = that.find("img").attr('src');
				//console.log(buttonImg);
				$('.item__details_img .button__style').attr('src', buttonImg);
			}
			
			// Когда меняется основная ткань нужно менять ткань воротника и манжета
			if (propCode == 'fabric') {
				
				var collarFabric = $('.construct_form .params__item.collar_fabric'),
					cuffFabric = $('.construct_form .params__item.cuff_fabric');
					
				
				if (fabricID == collarFabric.find(".param_input").val()) {
					collarFabric.find(".params__value").html(that.find(".item__name").html().trim());
					collarFabric.find(".param_input").val(that.data("val"));
				}
				if (fabricID == cuffFabric.find(".param_input").val()) {
					cuffFabric.find(".params__value").html(that.find(".item__name").html().trim());
					cuffFabric.find(".param_input").val(that.data("val"));
				}
			}
			
			
			sendAjaxToUpdateImg();
        }
    });
	
	$("body").on("click", "form .submenu  a", function() {
        $(this).closest(".submenu").siblings(".select__field").val($(this).text().trim());
		$(this).closest(".params__item").find(".param_input").val($(this).data("val"));
        $(this).closest(".submenu").hide().parent().removeClass("open");
        return false;
    });
	
	$("body").on("click", ".additional__options label.opt", function(e) {
		e.preventDefault();
		var chbox = $(this).siblings(".additional__options input[type='radio']"),
			gr = chbox.data("group"),
			code = chbox.data("code");
			optID = chbox.data("id");
			
		chbox.prop("checked", !chbox.prop("checked"));
		
		/*
		console.log(chbox);
		console.log(gr);//decor_add
		console.log(code);//dec_1
		console.log(optID);//312
		*/
		
		
		//$('.params__item.decor_add .select__field.dec_1').val('Да'); // - Визуал
		//$('input.param_input[name="decor_add"]').val(); // - Инпут
				if ($('.params__item.'+ gr).length > 1) {
					$('.params__item.'+ gr +' .select__field').val('Нет');
				}

				if (chbox.prop('checked') === true) {
					$('.params__item.'+ gr +' .select__field.' + code).val('Да');

					$('.params__item.'+ gr +' .select__field').attr('data-opt', 'N');
					$('.params__item.'+ gr +' .select__field.' + code).attr('data-opt', 'Y');
					$('input.param_input[name="'+ gr +'"]').val(optID);
				} else {
					$('.params__item.'+ gr +' .select__field.' + code).val('Нет');

					$('.params__item.'+ gr +' .select__field.' + code).attr('data-opt', 'N');
					$('input.param_input[name="'+ gr +'"]').val('0');
				}
				
				/* // for TO_DEL template
				if (chbox.prop('checked') === true) {
					$('[name="' + gr + '"]').attr('value', optID);
					$('.params__item.' + gr + ' .params__value').html($(this).html().trim());
				} else {
					$('[name="' + gr + '"]').attr('value', '0');
					$('.params__item.' + gr + ' .params__value').html('Нет');
				}
				*/
	
			sendAjaxToUpdateImg();
		
	});
	
	$("body").on("submit", ".construct_form", function(e) {
		e.preventDefault();
		
		if (!checkMonogramTextInput()) {
			alert('Заполните "Текст вышивки" на вкладке Детали-Вышивка');
			return false;
		}
		
		var emptyField_1 = false;
		var emptyField_2 = false;
		$('.size_form').find ('input, textearea, select').each(function() {
			//console.log(this.value);
			if (this.name == 'save_name' || this.name == 'profile_name') {
				//console.log('!!!save_name!!!');
			}else if (this.value.trim() == '') {
				//console.log(this.name + ' - Пустое значение!');
				emptyField_2 = true;
			}
		});
		
		if ($('[name="standart_size_growth"]').attr('value') == '' || $('[name="standart_size_neck"]').attr('value') == '') {
			emptyField_1 = true;
			//console.log('emptyField_1');
		}
		
		if ($('[name="size_profile"]').attr('value') == '') {
			//
		}

		if (!emptyField_1) {
			var sumOb = $(this).serialize() + '&' + $(".standard_size_form").serialize();
		} else if (!emptyField_2 || $('[name="save_wo_size"]').val() == 'Y') {
			var sumOb = $(this).serialize() + '&' + $(".size_form").serialize() + '&size_comment=' + $('[name="size_comment"]').val();
		} else {
			if ($('[href="#t4"]').hasClass('active')) {
				alert('Укажите все размеры');
			} else {
				$('[href="#t4"]').click();
			}
			return false;
		}

		//console.log(sumOb);
		//return false;
		
		$.ajax({
			url: location.href,
			//data: $(this).serialize(),
			data: sumOb,
			success: function(data) {
				if(data == "success") {
					location.href = "/personal/cart/"
				}
			}
		});
		
	});
	
	$("body").on("click", ".standard_size_form .submenu .submenu__item", function(e) {
		e.preventDefault();
		var that = $(this);
		var thatInput = $(this).closest(".select__item").find(".size_input");
		thatInput.val(that.attr('data-val'));

		if (thatInput.attr('name') == 'size_profile') {
			var curInput = thatInput.attr('value');
			if (curInput == 'default') {
				$('.size_form').find ('input, textearea, select').each(function() {
					$(this).attr('value', '');
					$(this).val('');
				});
			} else {
				var curProfile = arProfiles[curInput];
				// Заполняем поля формы размеров
				for (key in curProfile) {
					$('.size_form [name="' + key + '"]').attr('value', curProfile[key]);
					$('.size_form [name="' + key + '"]').val(curProfile[key]);
				}
				// Переход на другую вкладку
				//$('[href="#t4s2"]').click();
			}
		}
	});
	
	// Наше событие при успешной отправке формы "Записаться в ателье" на странице конструктора
	$("body").on("constructFeedback", function(e) {
		saveWOSize("Y");
		$(".construct_form").submit();
	});
	
	setIncompatibility(arIncom);
	
	$('.new__profile_link').click(function(){
		$('.secondary__tabs_link[href="#t4s2"]').click();
	});
	
	// Обработка инпута с текстом монограммы
	$('.embroidery-text [name="monogram_text"]').blur(function(event) {
		var monogramText = event.target.value;
		if (monogramText.length > 0) {
			$('.params__item.monogram_text .params__value').html(monogramText);
			$('.construct_form [name="monogram_text"]').attr('value', monogramText);
		} else {
			$('.params__item.monogram_text .params__value').html('Нет');
			$('.construct_form [name="monogram_text"]').attr('value', '0');
		}
	});
	
	// Проверка заполнения размеров
	$("body").on("click", "#t4s1 .submenu a.submenu__item", function () {
		if (checkSizeInput()) {
			$('.add-in-basket').addClass('red_back');
		} else {
			$('.add-in-basket').removeClass('red_back');
		}
	});
	
	$("body").on("keyup", "#t4s2 .size_form .left__content input", function () {
		if (checkSizeInput()) {
			$('.add-in-basket').addClass('red_back');
		} else {
			$('.add-in-basket').removeClass('red_back');
		}
	});
	
	$("body").on("click", '.primary__tabs_head .primary__tabs_link', function () {
		//console.log($(this).attr('href'));
		hideNextBtn($(this).attr('href'));
	});

	if (true) {
		//var optionsTest = 'construct_ajax=Y&save_wo_size=N&image=%2Fupload%2Fimg_shirts%2F806%2Fe925d4fef81c77c28d6adfd2a38ef160.png&price=5950&fabric=804&volume=260&clasp=274&collar=281&collar_add=0&sleeve=283&cuff=291&cuff_bottom=292&back=296&pocket=299&decor_add=0&collar_fabric=844&cuff_fabric=844&white_collar=0&white_cuff=0&buttons=773&embroidery_place=302&monogram=306&monogram_text=0&monogram_color=1015';
		//sendAjaxToUpdateImg(optionsTest);
		sendAjaxToUpdateImg('load');
	}
});

function checkSizeInput() {
	var size_standart = 'OK';
	var size_inputs = 'OK';

	$('.standard__size_select input.size_input').each(function() {
		if ($(this).val().length < 1) {
			size_standart = '';
			return false;
		}
	});
	
	$('#t4s2 .size_form .left__content input').each(function() {
		if ($(this).val().length < 1) {
			size_inputs = '';
			return false;
		}
	});

	if (size_standart != 'OK' && size_inputs != 'OK') {
		return false;
	}
	return true;
}

function checkMonogramTextInput() {
	if ($('.construct_form [name="embroidery_place"]').val() != 302 && $('.construct_form [name="monogram_text"]').val() == 0) {
		return false;
	} else {
		return true;
	}
}

function hideNextBtn(tabID) {
	if (tabID == 't4' || tabID == '#t4' || tabID == 't4s1' || tabID == 't4s2') {
		$('.js-next__step_btn').css(
			{visibility: 'hidden'}
		);
	} else {
		$('.js-next__step_btn').css(
			{visibility: 'visible'}
		);
	}
}














