$(document).ready(function() {
	$("body").on("click", "form .submenu  a", function() {
        $(this).closest(".submenu").siblings(".select__field").val($(this).text().trim());
		
		if($(this).hasClass("opt")) {
			var chbox = $(this).closest(".params__item").find(".param_input");
			chbox.prop("checked", !chbox.prop("checked"));
		} else {
			$(this).closest(".params__item").find(".param_input").val($(this).data("val"));
		}
		
        $(this).closest(".submenu").hide().parent().removeClass("open");
		
		if ($(this).attr('data-id-val')) {
			// Выпадашка
			$('[data-val="'+$(this).attr('data-id-val')+'"]').click();
			
		} else if ($(this).parent().attr('data-id-val')) {
			// Да/Нет
			
			//console.log($(this).attr('data-opt'));
			//console.log($(this).parent());
			//console.log($(this).parent().siblings('.select__field').val());
			var optValueChecked = $(this).closest(".submenu").siblings(".select__field").attr('data-opt'),
				optValue = $(this).attr('data-opt');
				
			if (optValueChecked != optValue) {
				$('[data-id="'+$(this).parent().attr('data-id-val')+'"]').siblings('label').click();
			}
		}
		
        return false;
    });
});