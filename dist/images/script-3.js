/*
function showClothesPopup() {
	$(".popups").show();
	$(".cloth__popup").show();
	$("body").addClass("popup__open");
	return false;
}

function initSlider() {
	if($('.popup__slider').length > 0) {
        $('.popup__slider').slick({
            fade: true,
            infinite: true,
            prevArrow: '<div class="banner__arrow banner__prev"></div>',
            nextArrow: '<div class="banner__arrow banner__next"></div>',
        });
    }
}

function getFabricData(itemID, func) {
	$.ajax({
		url: '/ajax/fabric_item.php',
		dataType: 'json',
		data: {
			ELEMENT_ID: itemID,
			AJAX_FABRIC: "Y"
		},
		success: function(data) {
			if($(data.HTML).hasClass("popup_item")) {
				$(".cloth__popup").html($(data.HTML).html());
				initSlider();
			}
			
			if(data.IMG && data.IMG.length > 0) {
				$(".zoom__btn").attr("href", data.IMG);
			}
			
			if(data.PRICE && parseInt(data.PRICE) > 0) {
				$(".price__info span").html(data.PRICE + " руб.");
			}
			
			if(data.NAME && data.NAME.length > 0) {
				$(".cloth__factory span").html(data.NAME);
				$(".cloth__info span").html(data.NAME);
			}
		}
	});
}

$(document).ready(function() {
    $("body").on("click", ".cloth__select .select__item  a", function() {
    	var that = $(this);
        that.closest(".submenu").siblings(".select__field").text(that.text());
        that.closest(".submenu").hide().parent().removeClass("open");
        return false;
    });

	// Загрузка инф. по выбранной ткани
	var activeItem = $(".cloth__item.active"),
		itemID = activeItem.find(".item__link").data("id");
	if(parseInt(itemID) > 0) {
		getFabricData(itemID);
	}
	
	$(document).on("click", ".item__link", function(e) {
		e.preventDefault();
		showClothesPopup();
	});
	
	// Дет. инф. по клику на ткань
	$(document).on("click", ".cloth__item", function(e) {
		e.preventDefault();
		var itemID = $(this).find(".item__link").data("id");
		getFabricData(itemID);
	});
});
*/
