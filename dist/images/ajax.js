"use strict";
/*
	data-iblock-id - id ИБ в который будут добавлятся запросы
	
	возможные классы для input или его прямого родителя
		user__name - имя пользователя
		user__mail - e-mail пользователя
		user__phone - телефон пользователя
		textarea - текстовое поле
		article__id - id статьи в блогах
		user__id - id пользователя
	
	чтобы поля проходили валидацию добавляем атрибут required, например:
	<div class="form__item user__name">
		<input type="text" placeholder="Введите ФИО" required>
	</div>
*/
//полифил для closest для IE
(function() {
  if (!Element.prototype.matches) {

    Element.prototype.matches = Element.prototype.matchesSelector ||
      Element.prototype.webkitMatchesSelector ||
      Element.prototype.mozMatchesSelector ||
      Element.prototype.msMatchesSelector;
  }
})();
(function() {

  if (!Element.prototype.closest) {

    Element.prototype.closest = function(css) {
      var node = this;

      while (node) {
        if (node.matches(css)) return node;
        else node = node.parentElement;
      }
      return null;
    };
  }
})();

document.addEventListener("DOMContentLoaded", pageStart);
let ajaxPath = "/ajax/formHeandler.php";
let ajaxFilePath = "/ajax/fileHeandler.php";

function pageStart(){

	let forms = document.querySelectorAll(".popups .popup form[data-iblock-id]");
	let popups = document.getElementsByClassName('popups')[0];
	let fileForSending = {};
	
	popups.addEventListener("click", function(event)  {
		if(!event.target.closest(".popup")){
		
			let popup = popups.querySelector('.popup[style="display: block;"]');
			
			if(popup && popup.querySelector("form[data-iblock-id]")){
				clearForm({target: popup});
			}
		}
	});

	for(let i = 0; i < forms.length; i++){
		forms[i].querySelector("button[type='submit']").addEventListener("click", clickSendButton, true);
		forms[i].closest(".popup").querySelector(".close__btn").addEventListener("click", clearForm);
		
		let inputFile = forms[i].querySelector("input[type='file']");
		if(inputFile){
			inputFile.addEventListener('change', fileCatcher);
		}
	}
	
	function clearForm(event){

		let popup = event.target.closest(".popup");
		let form = popup.querySelector("form");
		let mess = popup.querySelector(".certificates__order_head");
		fileForSending = {};
		unsetErrorMessage(form);
		
		if(mess){
			mess.parentElement.removeChild(mess);
		}
		
		let hiddenElem = document.querySelectorAll(".hidden-elem");
		
		for(let i = 0; i < hiddenElem.length; i++){
			hiddenElem[i].hidden = false;
			hiddenElem[i].classList.remove("hidden-elem");
		}
		
		let inputs = form.querySelectorAll("input");
			
		for(let i = 0; i < inputs.length; i++){
			
			inputs[i].value = "";
		}
		
		let textAreas = form.querySelectorAll("textarea");
		
		for(let i = 0; i < textAreas.length; i++){
			
			textAreas[i].value = "";
		}
		
		let addedFiles = form.getElementsByClassName('added__file');
		
		if(addedFiles){
			for(let i = 0; i < addedFiles.length; i++){
				
				addedFiles[i].textContent = "";
			}
		}
	}
	
	function clickSendButton(event){
		
		event.preventDefault();
		if(checkValidation(event.target)){
			sendForm(event.target);
		}
	}
	
	function sendForm(target){

		let form = target.closest('form');
		if(form.tagName !== "FORM")
			return;
		
		let xhr = new XMLHttpRequest();
		xhr.open("post", ajaxPath);
		xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
		
		xhr.onload = function(){
			
			let responseObj = JSON.parse(this.responseText);
	
			if(responseObj["ERROR_MESS"]){
			
				unsetErrorMessage(form);
				setErrorMessage(form, responseObj["ERROR_MESS"]);
			}
			else{
				let popup = form.closest(".popup");

				let elemCollection = popup.querySelectorAll("div");
				
				for(let i = 0; i < elemCollection.length; i++){
					elemCollection[i].hidden = true;
					elemCollection[i].classList.add("hidden-elem");
				}

				form.hidden = true;
				form.classList.add("hidden-elem");
				
				let p = document.createElement('p');
				p.textContent = "Ваше сообщение принято";
				p.classList.add("certificates__order_head");
				popup.appendChild(p);

				if(fileForSending.popupName === sendObj.popupName){
					fileAjaxSending(responseObj.ID, sendObj.iblockId);
				}
				
				document.body.dispatchEvent(new CustomEvent('constructFeedback'));
			}
		}
		
		let sendingEmail = form.dataset.email || "a.latigovskii@manao.by";

		let sendObj = {
			popupName: getPopupName(form),
			iblockId: form.dataset.iblockId,
			page: window.location.toString(),
			topic: getTopic(getPopupName(form)),
			mail_to: sendingEmail
		};
		//добавляем поля
		let name = getInputValue(form, "user__name");
		if(name)
			sendObj.name = name;
		
		let mail = getInputValue(form, "user__mail");
		if(mail)
			sendObj.mail = mail;
		
		let phone = getInputValue(form, "user__phone");
		if(phone)
			sendObj.phone = phone;
		
		let textarea = getInputValue(form, "textarea");
		if(textarea)
			sendObj.textarea = textarea;
		
		let articleId = getInputValue(form, "article__id");
		if(articleId)
			sendObj.articleId = articleId;
		
		let userId = getInputValue(form, "user__id");
		if(userId)
			sendObj.userId = userId;
		
		xhr.send(JSON.stringify(sendObj));
	}
	
	function fileCatcher(event){

		fileForSending.value = event.target.files[0];
		let form = event.target.closest("form");
		fileForSending.popupName = getPopupName(form);
	}
	
	function fileAjaxSending(id, iblockId){
		
		let xhr = new XMLHttpRequest();
		let form = new FormData();
		
		form.append("upload_file", fileForSending.value);
		form.append("ELEMENT_ID", id);
		form.append("IBLOCK_ID", iblockId);
		xhr.open("post", ajaxFilePath);
		xhr.send(form);
	}
	
	function checkValidation(target){
	
		let form = target.closest('form');
		unsetErrorMessage(form);

		let errorMessageObj = {
				error: false,
				errorStack: []
		};
		let reqInputs = form.querySelectorAll("input[required], textarea[required]");

		for(let i = 0; i < reqInputs.length; i++){
			
			if(reqInputs[i].classList.contains("user__name") || reqInputs[i].parentElement.classList.contains("user__name"))
				checkValidationName(reqInputs[i], errorMessageObj);
			if(reqInputs[i].classList.contains("user__mail") || reqInputs[i].parentElement.classList.contains("user__mail"))
				checkValidationMail(reqInputs[i], errorMessageObj);
			if(reqInputs[i].classList.contains("user__phone") || reqInputs[i].parentElement.classList.contains("user__phone"))
				checkValidationPhone(reqInputs[i], errorMessageObj);
			if(reqInputs[i].classList.contains("textarea") || reqInputs[i].parentElement.classList.contains("textarea"))
				checkValidationTextarea(reqInputs[i], errorMessageObj);
			if(reqInputs[i].classList.contains("checkbox") || reqInputs[i].parentElement.classList.contains("checkbox"))
				checkValidationCheckbox(reqInputs[i], errorMessageObj);
		}

		if(errorMessageObj.error)
			setErrorMessage(form, errorMessageObj);
		
		return !errorMessageObj.error;
		
		function checkValidationName(elem, errorMessageObj){

			if(elem.value === "" || elem.value.length < 3){
				errorMessageObj.error = true;
				errorMessageObj.errorStack.push("Поле 'Имя пользователя', короче 3 символов");
				setNotValid(elem, "user__name");
			}
			else{
			
				let match = elem.value.match(/[a-zA-ZА-Яа-я_ёЁ][a-zA-Z0-9А-Яа-яёЁ\s_\-]+/);
				
				if(!match){
					errorMessageObj.error = true;
					errorMessageObj.errorStack.push("Поле 'Имя пользователя', содержит запрещённые символы");
					setNotValid(elem, "user__name");
				}
				else if(match[0] != elem.value){
					errorMessageObj.error = true;
					errorMessageObj.errorStack.push("Поле 'Имя пользователя', содержит запрещённые символы");
					setNotValid(elem, "user__name");
				}
			}
				
			return errorMessageObj.error;
		}
		
		function checkValidationMail(elem, errorMessageObj){

			if(elem.value === ""){
				errorMessageObj.error = true;
				errorMessageObj.errorStack.push("Поле 'E-MAIL пользователя', не заполненно");
				setNotValid(elem, "user__mail");
			}
			else{
				let match = elem.value.match(/([\w\d\-\.]{1,})@([\w][\w\d\-]{1,})\.(\w{2,})/);

				if(!match){
					errorMessageObj.error = true;
					errorMessageObj.errorStack.push("Поле 'E-MAIL пользователя', не валидно");
					setNotValid(elem, "user__mail");
				}
				else if(match[0] != elem.value){
					errorMessageObj.error = true;
					errorMessageObj.errorStack.push("Поле 'E-MAIL пользователя', не валидно");
					setNotValid(elem, "user__mail");
				}
			}
			
			return errorMessageObj.error;
		}
		
		function checkValidationPhone(elem, errorMessageObj){

			let match = elem.value.match(/[\+1-9\(\)\-][\d\(\)\s\-]{7,}/);// /[\+\d\(\)\-][\d\(\)\s\-]{7,}/u
			
			if(!match){
				errorMessageObj.error = true;
				errorMessageObj.errorStack.push("Поле 'Телефон', содержит запрещённые символы, либо слишком короткое");
				setNotValid(elem, "user__phone");
			}
			else if(match[0] != elem.value){
				errorMessageObj.error = true;
				errorMessageObj.errorStack.push("Поле 'Телефон', содержит запрещённые символы, либо слишком короткое");
				setNotValid(elem, "user__phone");
			}
	
			return errorMessageObj.error;
		}
		
		function checkValidationTextarea(elem, errorMessageObj){

			if(elem.value === "" || elem.value.length < 10){
				errorMessageObj.error = true;
				errorMessageObj.errorStack.push("Текстовое поле, короче 10 символов");
				setNotValid(elem, "textarea");
			}
		}
		
		function checkValidationCheckbox(elem, errorMessageObj){

			if(!elem.checked) {
				errorMessageObj.error = true;
				errorMessageObj.errorStack.push("Не принято пользовательское соглашение");
				setNotValid(elem, "checkbox");
			}
			
			return errorMessageObj.error;
		}
	}
	
	function getInputValue(form, name){

		let elem = form.getElementsByClassName(name)[0];
		if(elem){
			if(elem.tagName && (elem.tagName == "INPUT" || elem.tagName == "TEXTAREA")){
				return elem.value.replace(/<\/?[^>]+(>|$)/g, "");
			}
			else if(elem.firstElementChild && (elem.firstElementChild.tagName == "INPUT" || elem.firstElementChild.tagName == "TEXTAREA"))
				return elem.firstElementChild.value.replace(/<\/?[^>]+(>|$)/g, "");
			else
				return null;
		}
		else
			return null;
	}
	
	function getPopupName(form){
	
		if(form.parentElement.className.indexOf("__popup") == -1)
			return "";
		else{
			for (let i = 0; i < form.parentElement.classList.length; i++) {
				if(form.parentElement.classList[i].indexOf("__popup") != -1){
					let matchArr = form.parentElement.classList[i].match(/([\d\w\-\_]+)__popup/i);
					if(matchArr[1])
						return matchArr[1];
				}
			}
		}
	}
	
	function setErrorMessage(form, message){
	
		let errorBlock = document.createElement('ul');
		errorBlock.classList.add("error-message");
		
		if(typeof message == "string"){
			let errorLi = document.createElement('li');
			errorBlock.appendChild(errorLi);
			errorLi.textContent = message;
		}
		else if(typeof message == "object" && message.error){
		
			for(let i = 0; i < message.errorStack.length; i++){
				let errorLi = document.createElement('li');
				errorBlock.appendChild(errorLi);
				errorLi.textContent = message.errorStack[i];
			}
		}
		form.parentElement.insertBefore(errorBlock, form.parentElement.firstChild);
	}
	
	function setNotValid(elem, class_name){
		
		if(elem.parentElement.classList.contains(class_name) )
			elem.parentElement.classList.add("not-valid");
		else if(elem.classList.contains(class_name))
			elem.classList.add("not-valid");
	}
	
	function unsetErrorMessage(form){

		let OldErrorBlock = form.parentElement.querySelectorAll(".error-message");
	
		for(let i = 0; i < OldErrorBlock.length; i++){
			OldErrorBlock[i].parentElement.removeChild(OldErrorBlock[i]);
		}
		
		let notValid = form.parentElement.querySelectorAll(".not-valid");

		for(let i = 0; i < notValid.length; i++){
			notValid[i].classList.remove("not-valid");
		}
	}
	
	function getTopic(popupName){
	
		let pageStr = "";
		let page = "";
		
		if(popupName === "tailor"){
			page = "tailor";
		}
		else if(popupName === "sorochki-feedback"){
			page = "sorochki-feedback";
		}
		else{
			page = window.location.toString();
			let posStart = page.indexOf('/', 7);
			
			if(posStart !== -1){
				let posFin = page.indexOf('/', posStart+1);
				
				if(posFin !== -1)
					page = page.substring(posStart+1, posFin);
				else{
					let posFin = page.indexOf('?', posStart+1);
					
					if(posFin !== -1){
						page = page.substring(posStart+1, posFin);
					}
					else{
						page = page.substring(posStart+1, page.length);
					}
				}		
			}
		}
		var anotherThemeName = false;
		switch(page){
			case "contacts":
				if (popupName == "appointment") {
					anotherThemeName = true;
					pageStr = "Запись в ателье - контакты";
				}
				else {
					pageStr = " (страница контакты)";
				}
			break;
			case "sorochki":
				pageStr = " (страница сорочки)";
			break;
			case "kostyumy":
				pageStr = " (страница костюмы)";
			break;
			case "obuv":
				pageStr = " (страница обуви)";
			break;
			case "sorochki-feedback":
				pageStr = " (запись в студию)";
			break;
			case "tailor":
				pageStr = " (Вызов портного)";
			break;
			case "sozdat-sorochku":
				if (popupName == "appointment") {
					anotherThemeName = true;
					pageStr = "Запись в ателье - конструктор";
				}
				else {
					pageStr = " (страница конструктора)";
				}
			break;
		}

		if (anotherThemeName) {
			return "Клиент заказал " + pageStr;
		}
		else {
			return "Клиент заказал обратный звонок" + pageStr;
		}
	}
}