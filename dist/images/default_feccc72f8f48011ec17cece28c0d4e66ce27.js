
; /* Start:"a:4:{s:4:"full";s:77:"/local/components/manao/main.auth/templates/.default/script.js?15372603661305";s:6:"source";s:62:"/local/components/manao/main.auth/templates/.default/script.js";s:3:"min";s:0:"";s:3:"map";s:0:"";}"*/
$(document).ready(function() {
	
    $("body").on("click", "#"+formId+" .js-sign-in__submit", function(e) {
		e.preventDefault();
		
		var formDataDesk = $('#'+formId+'').serialize() +'&auth_submit_button=Y';

		if($(this).hasClass("code")) {
			// Первое нажатие STEP_1
			formDataDesk = formDataDesk + '&step=1';
		} else {
			// Второе нажатие STEP_2
			formDataDesk = formDataDesk + '&step=2';
		}
		
		
		$.ajax({
			type: 'post',
			url: '/ajax/authForm.php',
			data: formDataDesk,
			success: function (data) {
				//console.log(data);
				$('#'+containerId+'').html(data);
				
				if ($('#'+formId+' [name="STEP_2"]').val() == 'Y') {
					$('#'+formId+' .js-sign-in__submit').removeClass("code");
					$('#'+formId+' .form__item.code').slideDown();
					//$('#'+formId+' .js-sign-in__submit').removeClass("code").siblings(".form__item.code").slideDown();
				}
				
				document.body.dispatchEvent(new CustomEvent("formAjaxCall", {
						detail: {
							popupId: containerId
						}
					})
				);
			}
		});
		
    });
	
});

// function refreshAuth(){
	// $.ajax({
		// type: 'post',
		// url: '/',
		// data: {main_auth_ajax: 'Y'},
		// success: function (data) {
			// console.log(data);
		// }
	// });
// }
	

/* End */
;; /* /local/components/manao/main.auth/templates/.default/script.js?15372603661305*/
