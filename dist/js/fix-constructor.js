/**
 * menu 830px
 */
$(function () {
    calculateNewScale();
    $(window).resize(function () {
        calculateNewScale();
    });
});

function calculateNewScale() {
    if ($(window).width() <= 830) {
        var percentageOn1 = $(window).width() / 1300;
        var initHeight = 684;
        $(".wrapper.constructor__wrapper:not(.is-mod) .main").css({
            "height": initHeight * percentageOn1 + "px",
            "-moz-transform": "scale(" + percentageOn1 + ")",
            "-webkit-transform": "scale(" + percentageOn1 + ")",
            "transform": "scale(" + percentageOn1 + ")"
        });
    } else {
        $(".wrapper.constructor__wrapper:not(.is-mod) .main").removeAttr("style");
    }
}

/**
 * fix constructor
 */
$(function () {
    /*Dropdown Menu*/
    $('body').on('click', '.js-toggle', function (e) {
        e.preventDefault();
        var elemID = $(this).data('target');
        $('[data-attr=' + elemID + ']').toggle();
    });

    $(document).click(function (e) {
        if (!$('.select').is(e.target) && $('.select').has(e.target).length === 0) {
            $('.select__drop').hide();
        };
    });

    $('body').on('click', '.select__link', function (e) {
        e.preventDefault();
        $(this).parent().siblings().find('.select__link').removeClass('is-active');
        $(this).addClass('is-active');
    });

    /*constructor__slider*/
    $('.constructor__options.constructor__slider').slick("slickSetOption", "infinite", false, false);
    $('.constructor__options.constructor__slider .slider__small_prev').attr('aria-disabled', 'true');
    $('.buttons .constructor__options.constructor__slider').slick("slickSetOption", {
        slidesToShow: 5,
        infinite: false
    }, true);

    /*buttons js-constructor__option*/
    $("body").on("click", ".constructor__options_b .js-constructor__option", function () {
        $(this).parent().siblings().find(".js-constructor__option").removeClass("active");
        $(this).addClass("active");
    });

    /**items disabled */
    checkAccess();
    $('body').on('change', ':radio, :checkbox', function () {
        checkAccess();
    })

    /**add-in-basket active */
    $('body').on('click', '.primary__tabs_link', function () {
        activeBasket();
    });

});

function checkAccess() {
    var el = $('[data-attr="access_denied"]');

    $(el).each(function () {
        var target = $(this).attr('data-target');
        if ($(this).prop('checked') === true) {
            $('[data-id=' + target + ']').addClass('is-disabled');
        } else {
            $('[data-id=' + target + ']').removeClass('is-disabled');
        }
    });
}

/**toggle class red_back to add-in-basket */
function activeBasket(elem) {
    if ($('.primary__tabs_item:last').is(':visible')) {
        $('.add-in-basket').addClass('red_back');
    } else {
        $('.add-in-basket').removeClass('red_back');
    }
}
/**
 * bug-list-27-02
 */
$(function () {
    $('body').on('click', '.js-next__step_btn, .js-prev__step_btn', function () {
        activeBasket();
        activeStepBtn();
    });
    $('body').on('click', '.primary__tabs_link', function () {
        activeStepBtn();
    });
    calculateScale();
    $(window).resize(function () {
        calculateScale();
    });
});
function activeStepBtn(elem) {
    if ($('.primary__tabs_item:last').is(':visible')) {
        $(".js-next__step_btn").css("visibility", "hidden");
    } else {
        $(".js-next__step_btn").removeAttr("style");
    }
}

function calculateScale() {
    if ($(window).width() <= 830) {
        var percentageOn1 = $(window).width() / 1540;
        var initHeight = 864;
        $(".wrapper.constructor__wrapper.is-mod .main").css({
            "height": initHeight * percentageOn1 + "px",
            "-moz-transform": "scale(" + percentageOn1 + ")",
            "-webkit-transform": "scale(" + percentageOn1 + ")",
            "transform": "scale(" + percentageOn1 + ")"
        });
    } else {
        $(".wrapper.constructor__wrapper.is-mod .main").removeAttr("style");
    }
}